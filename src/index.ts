import { Game } from './game.js';

let game;

window.onload = function () {
	game = new Game();
	game.start();
}